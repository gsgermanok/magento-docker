FROM debian:jessie

MAINTAINER  Giménez Silva Germán Alberto

ENV NGINX_VERSION 1.9.12-1~jessie

RUN apt-key adv --keyserver hkp://pgp.mit.edu:80 --recv-keys 573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62 \
	&& echo "deb http://nginx.org/packages/mainline/debian/ jessie nginx" >> /etc/apt/sources.list \
	&& apt-get update \
	&& apt-get install -y \
						ca-certificates \
						nginx=${NGINX_VERSION} \
						nginx-module-xslt \
						nginx-module-geoip \
						nginx-module-image-filter \
						gettext-base \
	&& rm -rf /var/lib/apt/lists/*

RUN apt-get update

RUN apt-get -y install php5-fpm php5-mhash php5-mcrypt php5-curl php5-cli php5-mysql php5-gd php5-xsl php5-json php5-intl php-pear php5-dev php5-common php-soap libcurl3 curl

RUN cd /etc/php5/fpm/conf.d \
    && ln -s ../../mods-available/mcrypt.ini \
    && cd /etc/php5/cli/conf.d \
    && ln -s ../../mods-available/mcrypt.ini

# RUN apt-get install -y mariadb-server mariadb-client

RUN mkdir /etc/nginx/sites-available

COPY nginx.conf /etc/nginx
COPY default.conf /etc/nginx/conf.d
COPY ./tmp/cli/php.ini /etc/php5/cli
COPY ./tmp/fpm/php.ini /etc/php5/fpm
# COPY magento.conf /etc/nginx/conf.d


RUN mkdir /var/www 
WORKDIR /var/www

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log

CMD ["nginx", "-g", "daemon off;"]

